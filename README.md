# SD-Feeder
SD-Feeder is a CLI and GUI module to "feed" [Stable Diffusion](https://github.com/AUTOMATIC1111/stable-diffusion-webui) from a CSV file.

SD-Feeder Features
-
* Python module 
* Runs as CLI program or with a GUI (window)
* SD processing via CSV file
* Batch processing of prompts
* Switching of SD-models per CSV line/prompt
* Integration of processing parameters in the PNG metadata
* Activate or deactivate CSV lines per line
* Run one line against all or a list of SD-models
* Output filenames prefixed with SD-model name
* SD-model references via not exact search word
* Repeat line processing
* Interrupt processing at any time from the GUI
* Skipping of SD-models at any time
* Autoload CSV file if changed
* Execute autoload immediately or after processing finished
* Displays available SD-models
* Shows CSV filename in the window title bar 
* Image output directory configurable
* Log intensity configurable (values 0-4)
* Stable Diffusion URL configurable
* SD "restore_faces" parameter automatically active (configurable)

Installation
-
* Install CUDA suitable for your Graphics Card
* Install Python and Stable Diffusion according to [Stable Diffusion webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)
* Run SD with the command line parameter --api
* Copy the file sdfeeder.py to an arbitrary directory
* Python must be in your path and associated with the file extension py
* Write a CSV file in the feeder format

Usage
-
Parameters:<br>
&nbsp;&nbsp;&nbsp;&nbsp;sdfeeder.py csv-filename<br>
&nbsp;&nbsp;&nbsp;&nbsp;Example:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sdfeeder.py example.csv<br>
<br>
Or drag & drop the CSV file onto sdfeeder.py<br>
<br>
CSV file in feeder format:<br>
Must contain following columns:<br>
active<br>
sd_model<br>
repeat<br>
prompt<br>
seed<br>
width<br>
height<br>
sampler_name<br>
cfg_scale<br>
steps<br>
n_iter<br>
batch_size<br>
negative_prompt<br>
Columns and values must be separated by comma<br><br>
Example<br>
```
active,sd_model,repeat,prompt,seed,width,height,sampler_name,cfg_scale,steps,n_iter,batch_size,negative_prompt
X,chilloutmix,,sepia photo native american girl in traditional gown,-1,1024,1024,Euler a,7,20,5,1,"(((ugly)))"
,"RealisticVision
Dreamlike",,Portrait of an old african woman in a traditional gown sitting at a campfire huts in the background,-1,1024,1024,Euler a,7,20,5,1,""
,*,,"van gogh painting underwater african woman in a red gown floating, sharks in the background",-1,1024,1024,Euler a,7,20,5,1,""
X,"ProtoGen
RealisticVision",,high resolution concept art of an apartment living room overlooking a large futuristic city with floor to ceiling windows and mid century modern furniture lots of plants cinematic lighting ,-1,1024,1024,Euler a,7,20,5,1,""
X,"ProtoGen
RealisticVision",,high-resolution 8 k photo Rome ((in ruins)) huge burning fire dark cloud sky ,-1,1024,1024,Euler a,7,20,5,1,""
```
<br>
The X in the column "active" means that the CSV line is active and will be processed by SD-Feeder.
If "active" does not contain anything, the line will not be processed.<br>
By filling the "repeat" column it is possible to run any line multiple times.

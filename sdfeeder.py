import queue
import tkinter as tk
import tkinter.scrolledtext as st
from tkinter import filedialog as fd
from PIL import Image, PngImagePlugin
from datetime import datetime
import threading
import time
import tempfile
import csv
import sys
import os
import uuid
import requests
import io
import base64
import webbrowser


class SDFeeder:

	def __init__(self, loglevel=1, outputdir=''):
		self.titlebase = 'SD Feeder (c) Andreas Niggemann, Speyer'
		self.version = '1.0.7'
		self.stdloglevel = loglevel
		self.baseurl = 'http://127.0.0.1:7860'
		self.interrupturl = self.baseurl + '/sdapi/v1/interrupt'
		self.modelsurl = self.baseurl + '/sdapi/v1/sd-models'
		self.optionsurl = self.baseurl + '/sdapi/v1/options'
		self.imageurl = self.baseurl + '/sdapi/v1/txt2img'
		self.pnginfourl = self.baseurl + '/sdapi/v1/png-info'
		self.csvdelimiter = ','
		self.modellist = []
		self.logtextarea = None
		self.logfilenametextarea = None
		self.root = None
		self.autorefresh = None
		self.immediate = None
		self.skipmodel = None
		self.refreshtime = 5000
		self.generatorthread = None
		self.imgsavethread = None
		self.processqueue = None
		self.stopgenerating = None
		self.csvlastchangedtime = -1
		self.restorefacesactive = True
		self.useuuidforoutfile = False  # False -> use DateTime
		self.datetimeformat = "%Y%m%d%H%M%S"
		self.filenumber = 0
		self.currentfnameprefix = ''
		self.filename = ''
		self.withgui = False
		self.selectcsvfileatstart = False  # True -> Display CSV file selector at program start
		self.outputdirectory = outputdir.strip()
		if self.outputdirectory == '':
			self.outputdirectory = tempfile.gettempdir() + os.sep + 'SD_BATCH'
		if not os.path.exists(self.outputdirectory):
			os.mkdir(self.outputdirectory)

	#
	# This block is only for GUI
	#
	def showgui(self):
		self.stopgenerating = threading.Event()
		self.skipmodel = threading.Event()
		self.stopgenerating.clear()
		self.skipmodel.clear()
		self.root = tk.Tk()
		self.root.title(self.titlebase + ' V' + self.version)
		self.root.geometry('680x460')
		self.root.resizable(False, False)
		self.autorefresh = tk.IntVar()
		self.immediate = tk.IntVar()
		self.logtextarea = st.ScrolledText(self.root, wrap=tk.NONE, width=80, height=16, font=('Courier New', 10))
		self.logtextarea.grid(column=0, row=0, pady=2, padx=10, columnspan=7)
		self.logfilenametextarea = st.ScrolledText(self.root, wrap=tk.NONE, width=80, height=10, font=('Courier New', 10))
		self.logfilenametextarea.grid(column=0, row=1, pady=2, padx=10, columnspan=7)
		tk.Button(self.root, width=12, text='Open CSV file...', command=self.selectcsvfile).grid(column=0, row=2)
		tk.Button(self.root, width=8, text='About...', command=self.aboutwindow).grid(column=1, row=2)
		tk.Checkbutton(self.root, width=20, text='Load CSV file after change', variable=self.autorefresh).grid(column=2, row=2)
		tk.Checkbutton(self.root, width=8, text='Immediate', variable=self.immediate).grid(column=3, row=2)
		tk.Button(self.root, width=12, text='Stop generating', command=self.stopgenerator).grid(column=4, row=2)
		tk.Button(self.root, width=10, text='Skip model', command=self.skipcurrentmodel).grid(column=5, row=2)
		tk.Button(self.root, width=10, text='Exit', command=self.exitsdfeeder).grid(column=6, row=2)
		self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
		self.root.after(self.refreshtime, self.checkrefresh)
		if self.selectcsvfileatstart:
			self.selectcsvfile()
		self.withgui = True
		self.root.mainloop()

	def on_closing(self):
		self.exitsdfeeder()

	@staticmethod
	def aboutwindow():
		webbrowser.open_new(r"https://gitlab.com/ANiggemann/sd-feeder")

	def skipcurrentmodel(self):
		self.skipmodel.set()
		try:
			requests.post(self.interrupturl, json=None, timeout=5)
		except:
			pass
		self.logoutput('Skip model requested', 4)

	def stopgenerator(self):
		self.stopgenerating.set()
		try:
			requests.post(self.interrupturl, json=None, timeout=5)
		except:
			pass
		self.logoutput('Stop requested', 4)

	def exitsdfeeder(self):
		self.stopgenerator()
		self.root.withdraw()
		while self.generatorthread is not None and self.generatorthread.is_alive():
			self.root.update()
		sys.exit()

	def generatorsiderunner(self):
		self.process_one_csv(self.filename)
		self.stopgenerating.clear()
		self.skipmodel.clear()

	def textareaoutput(self, line, area=0):
		tarea = self.logtextarea if area == 0 else self.logfilenametextarea
		tarea.configure(state='normal')
		tarea.insert(tk.END, line + '\n')
		tarea.see(tk.END)
		tarea.configure(state='disabled')

	def imgsavesiderunner(self):
		while True:
			self.root.update()
			if not self.processqueue.empty():
				response = self.processqueue.get()
				self.saveimagesviaapi(response)

	def checkrefresh(self):
		if not self.stopgenerating.is_set() and self.generatorthread is not None:
			if self.autorefresh.get() == 1:
				if not self.generatorthread.is_alive():
					if self.memorizefilechangetime():
						self.generatorstart()
			if self.immediate.get() == 1 and self.memorizefilechangetime():
				self.stopgenerator()
				while self.generatorthread.is_alive():
					self.root.update()
				self.generatorstart()
		self.root.after(self.refreshtime, self.checkrefresh)

	def selectcsvfile(self):
		self.selectcsvfileatstart = False
		if self.generatorthread is None or not self.generatorthread.is_alive():
			self.filename = fd.askopenfilename(title='Open CSV file', filetypes=[('csv files', '*.csv')])
			if self.filename != '':
				self.generatorstart()

	def memorizefilechangetime(self):
		retval = False
		if os.path.isfile(self.filename) and os.access(self.filename, os.R_OK):
			filetime = os.stat(self.filename).st_mtime
			if self.csvlastchangedtime != filetime:
				self.csvlastchangedtime = filetime
				retval = True
		return retval

	def cleartextareas(self):
		self.logfilenametextarea.configure(state='normal')
		self.logfilenametextarea.delete(1.0, tk.END)
		self.logfilenametextarea.configure(state='disabled')
		self.logtextarea.configure(state='normal')
		self.logtextarea.delete(1.0, tk.END)
		self.logtextarea.configure(state='disabled')

	def generatorstart(self):
		self.cleartextareas()
		self.stopgenerating.clear()
		self.imgsavethread = threading.Thread(target=self.imgsavesiderunner, daemon=True)
		self.processqueue = queue.Queue()
		self.imgsavethread.start()
		self.generatorthread = threading.Thread(target=self.generatorsiderunner, daemon=True)
		self.memorizefilechangetime()
		self.generatorthread.start()

	def logoutput(self, line, loglevel=1, area=0):
		if self.stdloglevel >= loglevel:
			if self.withgui:
				self.textareaoutput(line, area)
			else:
				print(line)

	#
	# End of GUI block
	#

	# Get model list
	def getmodellist(self):
		mlist = []
		try:
			response = requests.get(self.modelsurl, timeout=20)
		except:
			pass
		else:
			for model in response.json():
				mlist.append(model.get('model_name'))
			mlist = sorted(mlist, key=str.casefold)
			self.logoutput('Available SD-Models:', 2)
			for model in mlist:
				self.logoutput('  ' + model, 2)
			self.logoutput('', 2)
		return mlist

	# Read CSV file
	def read_csv_file(self, csvfile):
		with open(csvfile) as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=self.csvdelimiter, quotechar='"')
			csvl = []
			for row in csv_reader:
				csvl.append(row)
			return csvl

	def findmodel(self, searchword):
		modelname = ''
		for line in self.modellist:
			if line.lower().find(searchword.lower()) > -1:
				modelname = line
				self.logoutput(searchword + ' -> ' + modelname, 3)
		return modelname

	def getnewfilename(self):
		self.filenumber += 1
		return str(uuid.uuid4()) if self.useuuidforoutfile else datetime.now().strftime(self.datetimeformat) + '_' + str(self.filenumber)

	def saveimagesviaapi(self, apiresponse):
		for i in apiresponse['images']:
			if self.withgui and (self.stopgenerating.is_set() or self.skipmodel.is_set()):
				self.skipmodel.clear()
				break
			image = Image.open(io.BytesIO(base64.b64decode(i.split(",", 1)[0])))
			png_payload = {"image": "data:image/png;base64," + i}
			pngresponse = requests.post(self.pnginfourl, json=png_payload)
			pnginfo = PngImagePlugin.PngInfo()
			pnginfo.add_text("parameters", pngresponse.json().get("info"))
			outputfilename = self.outputdirectory + os.sep + self.currentfnameprefix + self.getnewfilename() + '.png'
			image.save(outputfilename, pnginfo=pnginfo)
			self.logoutput(outputfilename, 1, area=1)

	def getimgviaapi(self, jsonpayload, fnamepre):
		self.logoutput(str(jsonpayload), 5)
		response = requests.post(self.imageurl, json=jsonpayload)
		r = response.json()
		self.currentfnameprefix = fnamepre
		if self.withgui:
			self.processqueue.put(r)
		else:
			self.saveimagesviaapi(r)

	def setmodelviaapi(self, model):
		self.logoutput('SD-Model: ' + model, 2)
		option_payload = {"sd_model_checkpoint": model}
		requests.post(self.optionsurl, json=option_payload)

	def appendmodels(self, modelormodellist, requestdict):
		dictlist = []
		modlistseparator = '\n'
		specialmodellist = []
		if modelormodellist.strip().lower() in ['*', 'all', 'alle']:
			specialmodellist = self.modellist
		else:
			if modelormodellist.find(modlistseparator) > 0:
				specialmodellist = modelormodellist.split(modlistseparator)
			else:
				specialmodellist.append(modelormodellist)
		for model in specialmodellist:
			rd = requestdict.copy()
			rd['sd_model'] = model.strip()
			dictlist.append(rd)
		return dictlist

	def fillrequestlist(self, csvlist):
		rqlist = []
		headers = []
		rdict = {}
		sd_mod = ''
		activeheader = -1
		for li in range(len(csvlist)):
			if li == 0:
				headers = [col.lower() for col in csvlist[li]]
				activeheader = headers.index('active')
			else:
				line = csvlist[li]
				repeatgenerator = 1
				if activeheader > -1 and line[activeheader] != '':
					for i in range(len(line)):
						fieldstripped = line[i].strip()
						if headers[i] == 'sd_model' and fieldstripped != '':
							sd_mod = fieldstripped
						else:
							if headers[i] == 'repeat' and fieldstripped != '':
								repeatgenerator = int(fieldstripped)
							else:
								if fieldstripped != '' and headers[i] != 'active' and headers[i] != 'repeat':
									if fieldstripped.lstrip('-+').isnumeric():
										rdict[headers[i]] = int(fieldstripped)
									else:
										if isinstance(fieldstripped, str):
											rdict[headers[i]] = fieldstripped
					if self.restorefacesactive:
						rdict['restore_faces'] = True
					rdict['repeat'] = repeatgenerator
					for dic in self.appendmodels(sd_mod, rdict):
						rqlist.append(dic)
		return rqlist

	def generate_img(self, model, payload, fnameprefix, repeatgen):
		sd_m = self.findmodel(model)
		if sd_m != '':
			self.setmodelviaapi(sd_m)
			for rep in range(repeatgen):
				if self.withgui and (self.stopgenerating.is_set() or self.skipmodel.is_set()):
					break
				self.getimgviaapi(payload, fnameprefix)
		else:
			self.logoutput('Model not found: ' + model, 1)

	def process_one_csv(self, csv_filename):
		if self.withgui:
			self.root.title(self.titlebase + ' V' + self.version + ' - ' + csv_filename)
		else:
			self.logoutput('CSV filename: ' + csv_filename, 1)
		self.modellist = self.getmodellist()
		csvlist = self.read_csv_file(csv_filename)
		requestlist = self.fillrequestlist(csvlist)
		oldprompt = ''
		oldmodel = ''
		for rq in requestlist:
			if self.withgui and self.stopgenerating.is_set():
				break
			repeatgenerator = rq['repeat']
			sd_mod = rq['sd_model']
			if sd_mod != oldmodel:
				oldmodel = sd_mod
				self.logoutput('Process with model: ' + sd_mod, 2)
			if rq['prompt'] != oldprompt:
				oldprompt = rq['prompt']
				self.logoutput('Prompt: ' + rq['prompt'], 2)
			del rq['sd_model']
			del rq['repeat']
			self.generate_img(sd_mod, rq, sd_mod + '_', repeatgenerator)
		self.logoutput("Aborted\n" if self.withgui and self.stopgenerating.is_set() else "Processed\n", 2)

	def process_loop(self, csvf):
		while True:
			try:
				if os.path.isfile(csvf) and os.access(csvf, os.R_OK):
					time.sleep(5)
					filetime = os.stat(csvf).st_mtime
					while self.csvlastchangedtime != filetime:
						self.csvlastchangedtime = filetime
						self.process_one_csv(csvf)
			except:
				time.sleep(5)


#
# Main
#
def main():
	prg_namebase, prg_ext = os.path.splitext(sys.argv[0])
	same_name_csv = prg_namebase + '.csv'
	sdfeeder = SDFeeder(3)
	if len(sys.argv) > 1:
		arg_file = sys.argv[1]
		if os.path.isfile(arg_file):
			sdfeeder.process_loop(arg_file)
	else:
		if os.path.isfile(same_name_csv):
			sdfeeder.process_loop(same_name_csv)
		else:
			sdfeeder.showgui()


if __name__ == "__main__":
	main()
